# My Magic Prompt

To launch the project you need to execute this command :
./main.sh

In a second time you need to enter your username and your password to connect in the prompt

If you are connected to the prompt you can use all this commands :

help : who show you all the command you can use

ls : list all files and folders visibles and hides

rm : delete a file

rmd ou rmdir : delete a folder

about : a description of the program

version ou --v ou vers : get version of the prompt

age : ask your age and say if you are minor or major

quit : exit you of the prompt 

profile : show your informations : First Name, Last name, age, email

passw : change your password 

cd : go to folder 

pwd : actual folder

hour : get hour

httpget : download source code html of a website

smtp : send a mail

open : open a file

