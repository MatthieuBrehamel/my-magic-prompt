#!/bin/bash

# Read content of .bash-profile
source .bash-profile

# cmd function she read all command enter in prompt with a infinite while and a case inside
cmd() {
  while [[ 1 ]]; do
    echo "matthieu ~ "
    read cmd arg1

    case "$cmd" in
    pwd) pwd ;;
    passw) passw "$arg1" ;;
    ls) ls -a "$arg1" ;;
    version | --v | vers) echo "1.0" ;;
    hour) date +%H:%M ;;
    cd ) cd "$arg1" || cmd ;;
    rm ) rm "$arg1" || cmd ;;
    rmdir | rmd ) rmdir "$arg1" || cmd ;;
    open ) vim "$arg1" ;;
    profile) echo "Matthieu Brehamel 21 ans à l'adresse brehamel.matthieu@hotmail.com" ;;
    help) echo "Vous pouvez utiliser les commandes suivantes :
      help : qui indiquera les commandes que vous pouvez utiliser
      ls : lister les fichiers et les dossiers visibles comme cachés
      rm : supprimer un fichier
      rmd ou rmdir : supprimer un dossier
      about : une description du programme
      version ou --v ou vers :  affiche la version du magic prompt
      age : vous demande votre âge et vous dit si vous êtes majeur ou mineur
      quit : permet de sortir du magic prompt
      profile : permet d’afficher toutes les informations sur vous même.
      Prénom, Nom, âge et email
      passw : permet de changer le mot de passe avec une demande de confirmation
      cd : aller dans un dossier que vous venez de créer ou de revenir à un dossier
      précédent
      pwd : indique le répertoire actuel courant
      hour : permet de donner l’heure actuelle
      httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique.
      smtp : vous permet d’envoyer un mail avec une adresse un sujet et le corp du mail
      open : ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas
    " ;;
    age) age ;;
    smtp) smtp ;;
    httpget) httpget ;;
    about) echo "A little magic prompt !
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣤⣤⣤⣤⣤⣶⣦⣤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⡿⠛⠉⠙⠛⠛⠛⠛⠻⢿⣿⣷⣤⡀⠀⠀⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⠀
            ⣼⣿⠋⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⠈⢻⣿⣿⡄⠀⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⣸⣿⡏⠀⠀⠀⣠⣶⣾⣿⣿⣿⠿⠿⠿⢿⣿⣿⣿⣄⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀
          ⣿⣿⠁⠀⠀⢰⣿⣿⣯⠁⠀⠀⠀⠀⠀⠀⠀⠈⠙⢿⣷⡄⠀ ⠀⠀⣀⣤⣴⣶⣶⣿⡟⠀⠀⠀⢸⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣷⠀ ⠀
    ⢰⣿⡟⠋⠉⣹⣿⡇⠀⠀⠀⠘⣿⣿⣿⣿⣷⣦⣤⣤⣤⣶⣶⣶⣶⣿⣿⣿⠀ ⠀ ⢸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠃⠀ ⠀
    ⣸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠉⠻⠿⣿⣿⣿⣿⡿⠿⠿⠛⢻⣿⡇⠀⠀ ⠀ ⣿⣿⠁⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣧⠀⠀ ⠀
    ⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀ ⠀ ⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀ ⠀
    ⢿⣿⡆⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡇⠀⠀ ⠀ ⠸⣿⣧⡀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⠃⠀⠀ ⠀⠀
      ⠛⢿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⣰⣿⣿⣷⣶⣶⣶⣶⠶⠀⢠⣿⣿⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⣽⣿⡏⠁⠀⠀⢸⣿⡇⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀
            ⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⢹⣿⡆⠀⠀⠀⣸⣿⠇⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀⢿⣿⣦⣄⣀⣠⣴⣿⣿⠁⠀⠈⠻⣿⣿⣿⣿⡿⠏⠀⠀⠀⠀ ⠀⠀⠀⠀⠀⠀⠀
            ⠈⠛⠻⠿⠿⠿⠿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀" ;;
    quit | exit) exit 0 ;;
    *) cmdNotFound ;;
    esac

  done
}

# main function who call login function
main() {
  login
}

# age function ask you to write your age and she echo if you are minor or major
age() {
  read -p 'How old are you ?' number

  if [ "$number" -lt 18 ]; then
    echo "Vous êtes mineur."
  else
    echo "Vous êtes majeur."
  fi
}

# smtp function ask you to enter the email address, the object and the content and she sent the mail
smtp() {
  read -p 'Email ?' email
  read -p 'Object ?' object
  read -p 'Content ?' content

  echo "$content" | mail -s "$object" "$email"
  echo "Mail envoyé !"

  cmd
}

# httpget function ask you to enter the link of the website and the filename where is stock the http
httpget() {
  read -p 'Lien du site ?' link
  read -p 'Nom du fihcier ?' filename

  curl "$link" >> ./"$filename".txt
  echo "Http téléchargé dans ""$filename"".txt"

  cmd
}

# passw function allow you to change your password
passw() {
  read -p 'Etes vous sur [oui ou non] ?' answer

  if [ "$answer" = "oui" ] || [ "$answer" = "o" ] || [ "$answer" = "O" ]; then
      sed -i "" "s/$PASSWORD/$1/g" .bash-profile
  else
     echo "abandon."
  fi
}

# login function is call at the start of the prompt because you need to log in to write command
login() {
  read -p 'Login: ' uservar
  read -sp 'Password: ' passvar

  if [ "$uservar" = "root" ] && [ "$passvar" = "$PASSWORD" ]; then
     echo "Vous êtes connecté."
     cmd
  else
     echo "Mauvais identifiants."
     login
  fi
}

# call main function
main
